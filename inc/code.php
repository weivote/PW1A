<?php
 /**
 * code.php PW1A:a591bd3ab90c7c548f70569ad83193f620221114181244
 * Created by yichaxin.com V221114_18124320221114181244
 * @author yujianyue<admin@12391.net>
 * 注意：著作权所有,不得公开发布和出售; 源码免费提供,自己评估安全性，作者不承担任何后果！
 * 以自用为目的，在保留版权标识的前提下可将本系统用于盈利或非盈利项目上
 * 问题反馈：15058593138 同微信号 沟通请提供版本号PW1A
 * 提示：如果本款不适合，可访问http://12391.net免费下载更适合版本。
 * 赞助：如果好用，欢迎捐赠我们:http://juan.12391.net/
 * 帮助：源码自带说明书，详见压缩包内*.html/*.txt文件(都不建议删除)。
 * 保留：原始数据也请保留；供以后规律参考(比如看前几行都是什么)。
 * 技巧：原始版本能用而你修改后不能用，说明是你操作问题(建议保留原始版本)。
 * 通用：显示结果均你数据决定；即所下载的查询都是发出简单灵活的通用查询；
 * Date: 221114@6372149b81ddf 
 */
error_reporting(0);
session_start();
getCode(4,50,20);
function getCode($num,$w,$h) {
$code = "";
for ($i = 0; $i < $num; $i++) {
$code .= rand(0, 9);
}
//4位验证码也可以用rand(1000,9999)直接生成
//将生成的验证码写入session，备验证页面使用
$_SESSION['Code20221114181244'] = $code;
setcookie("Code20221114181244", md5("20221114181244#20221114181244"), time()+1200);
//创建图片，定义颜色值
Header("Content-type: image/PNG");
$im = imagecreate($w, $h);
$black = imagecolorallocate($im, 255, 0, 63);
$gray = imagecolorallocate($im, 200, 200, 200);
$bgcolor = imagecolorallocate($im, 255, 255, 255);
imagefill($im, 0, 0, $bgcolor);
//画边框
//imagerectangle($im, 0, 0, $w-1, $h-1, $black);
//在画布上随机生成大量黑点，起干扰作用;
for ($i = 0; $i < 10; $i++) {
imagesetpixel($im, rand(0, $w), rand(0, $h), $black);
}
//将数字随机显示在画布上,字符的水平间距和位置都按一定波动范围随机生成
$strx = rand(1,3);
for ($i = 0; $i < $num; $i++) {
$strpos = rand(2, 6);
imagestring($im, 5, $strx, $strpos, substr($code, $i, 1), $black);
$strx += rand(8, 12);
}
imagepng($im);
imagedestroy($im);
}

 // 也有更多付费版本供选购，区别：细节更佳，提供使用指导 
 //code.php?t=221114@6372149b824a1&m=a591bd3ab90c7c548f70569ad83193f6 
?>